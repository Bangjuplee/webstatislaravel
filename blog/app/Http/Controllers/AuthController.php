<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    Public Function form(){
        return view('form');
    }

    Public Function selamatdatang(Request $request){
        $namadepan = $request['firstname'];
        $namabelakang = $request['lastname'];
        return view('welcome', compact('namadepan','namabelakang'));
    }

}
